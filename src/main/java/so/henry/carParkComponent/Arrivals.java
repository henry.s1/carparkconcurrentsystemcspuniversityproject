package so.henry.carParkComponent;


import org.jcsp.lang.CSProcess;
import org.jcsp.lang.ChannelInput;
import org.jcsp.lang.ChannelInputInt;
import org.jcsp.lang.ChannelOutput;
import org.jcsp.lang.ChannelOutputInt;

import so.henry.bookingComponent.ETicket;

public class Arrivals implements CSProcess
{
	ChannelOutput outputTicket;
	ChannelInput inputTicket;
	public Arrivals(ChannelOutput eOut, ChannelInput eIn)
	{
		this.outputTicket = eOut;
		this.inputTicket = eIn;
	}
	
	@Override
	public void run()
	{
		while(true)
		{			
			ETicket tempTicket = (ETicket) inputTicket.read();
			outputTicket.write(tempTicket);
		}
		
	}

}
