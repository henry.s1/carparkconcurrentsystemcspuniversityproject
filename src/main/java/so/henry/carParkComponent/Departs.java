package so.henry.carParkComponent;

import org.jcsp.lang.CSProcess;
import org.jcsp.lang.ChannelInput;
import org.jcsp.lang.ChannelOutput;

import so.henry.bookingComponent.ETicket;

public class Departs implements CSProcess
{		
    ChannelInput inTicket;
    ChannelOutput outTicket;
	public Departs(ChannelInput inTicket, ChannelOutput outTicket)
	{
		this.inTicket = inTicket;
		this.outTicket = outTicket;
	}
	
	@Override
	public void run()
	{
		while(true)
		{
			ETicket tempTicket = (ETicket) inTicket.read();
			outTicket.write(tempTicket);
		}
		
	}

}
