package so.henry.carParkComponent;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.jcsp.lang.Alternative;
import org.jcsp.lang.AltingChannelInput;
import org.jcsp.lang.AltingChannelInputInt;
import org.jcsp.lang.CSProcess;
import org.jcsp.lang.ChannelInput;
import org.jcsp.lang.ChannelInputInt;
import org.jcsp.lang.ChannelOutput;
import org.jcsp.lang.ChannelOutputInt;
import org.jcsp.lang.Guard;

import so.henry.bookingComponent.ETicket;

public class Control implements CSProcess
{

    AltingChannelInput request; // from departures for alting 
    ChannelOutput outTicket;
    AltingChannelInput inTicket;
    ChannelInput depTicket;
    
    ArrayList<ETicket> ticket = new ArrayList<ETicket>();
	int spaces = 1;// spaces
	
	
	public Control(AltingChannelInput request, AltingChannelInput inTicket, 
			ChannelOutput outTicket, ChannelInput depTicket)
	{
		this.request = request;
		this.inTicket = inTicket;
		this.outTicket = outTicket;
		this.depTicket = depTicket;
	}
	
	
	@Override
	public void run()
	{
		
		final Guard[] altChannels = {inTicket, request}; 
		final Alternative alt = new Alternative(altChannels);
		while(true)
		{
			switch(alt.fairSelect())
			{
				case 0: // arrival					
					if(spaces > 0)
					{
						ETicket tempTicket = (ETicket) inTicket.read(); // not work?
						System.out.println(tempTicket.getRef());
						
						outTicket.write(tempTicket);// send eticket to depart
					}		
					break;
				case 1: // departure
					if(spaces < 1) 
					{
						ETicket departTicket = (ETicket) depTicket.read();
						int duration =  departTicket.getDuration();
						
						for(int j = duration; j > 0 ;j--)
						{
							System.out.println("vehicle leaves in: " + j);
							try // this breaks at some point
							{
								TimeUnit.SECONDS.sleep(1); // wait for user duration then allow new ticket
							} 
							catch (InterruptedException e)
							{
								//TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						spaces++; // add one to free space						
					}
					break;
			}
		}		
	}
}
