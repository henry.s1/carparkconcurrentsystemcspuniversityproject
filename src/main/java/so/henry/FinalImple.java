package so.henry;

import org.jcsp.lang.*;

import so.henry.bookingComponent.Booking;
import so.henry.mailToolComponent.Dispatch;
import so.henry.mailToolComponent.InputControl;
import so.henry.mailToolComponent.MailBag;
import so.henry.carParkComponent.Arrivals;
import so.henry.carParkComponent.Control;
import so.henry.carParkComponent.Departs;


public class FinalImple
{

	public static void main(String[] args)
	{

		System.out.println("hello world please print this");
		One2OneChannel arrtocon = Channel.one2one(); // communicate between arrival out and control in
		One2OneChannel contodep = Channel.one2one(); // communicate between control out and depature in
		One2OneChannel deptocon = Channel.one2one();	// communicate between departure out and control in	
		One2OneChannel booktomail = Channel.one2one(); // communicate between booking out and mail in
		One2OneChannel mailtoinput = Channel.one2one(); // communicate between mail out and input in
		One2OneChannel inputtodispatch = Channel.one2one(); // communicate between input out and dispatch in
		One2OneChannel dispatchtoarrival = Channel.one2one(); // communicate between dispatch out and arrival in
		
		Parallel FinalImple;
		
		
		FinalImple = new Parallel(
				new CSProcess[]
						{
								new Arrivals(arrtocon.out(), dispatchtoarrival.in()),
								new Control(deptocon.in(), arrtocon.in(), contodep.out(), deptocon.in()),
								new Departs(contodep.in(), deptocon.out()),
								new Booking(booktomail.out()),
								new MailBag(mailtoinput.out(), booktomail.in()),
								new Dispatch(inputtodispatch.in(), dispatchtoarrival.out()),
								new InputControl(mailtoinput.in(), inputtodispatch.out())
						});
		FinalImple.run();
		

	}

}
