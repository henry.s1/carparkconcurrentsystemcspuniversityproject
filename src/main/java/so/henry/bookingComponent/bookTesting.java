package so.henry.bookingComponent;

import org.jcsp.lang.CSProcess;
import org.jcsp.lang.Channel;
import org.jcsp.lang.One2OneChannel;
import org.jcsp.lang.Parallel;

public class bookTesting
{

	public static void main(String[] args)
	{
		One2OneChannel bookingtotest = Channel.one2one();
		
		Parallel bookTesting;
		
		bookTesting = new Parallel(
				new CSProcess[] 
						{
								new Booking(bookingtotest.out()),
								new BookingOutputTest(bookingtotest.in())
						});
		bookTesting.run();
	}

}
