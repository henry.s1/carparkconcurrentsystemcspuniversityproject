package so.henry.bookingComponent;


import java.awt.Frame;
import java.awt.Label;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.jcsp.lang.CSProcess;
import org.jcsp.lang.ChannelOutput;

public class Booking implements CSProcess
{
	ChannelOutput sendT;	
	
	public Booking(ChannelOutput eOut)
	{
		this.sendT = eOut;
	}

	@Override
	public void run()
	{
		int i = 1;
		while(true)
		{
			//ETicket tempT = ticketList.get(i); // for debugging
			//Scanner in = new Scanner(System.in); // for debugging
			String name = JOptionPane.showInputDialog("Please enter booking ref "); //in.nextLine();

			//System.out.println("Enter ticket duration"); // for debugging
			String dur = JOptionPane.showInputDialog("Please enter duration");//in.nextLine(); // for debugging
			int duration = 1;
			try
			{

				duration = Integer.parseInt(dur);
			}
			catch(NumberFormatException e)
			{
				duration = 1;
				System.err.println("not a number, duration set to 1"); // for debugging
			}
			
			ETicket tempT = new ETicket(name, duration);
			
			
			sendT.write(tempT);
			i--;
			//System.out.println("Vehicle In Carpark. Spaces: " + i); // for debugging
			
			for(int j = duration; j > 0 ;j--)
			{
				//System.out.println("vehicle leaves in: " + j); // for debugging
				Frame f=new Frame("Parking display");
				Label label1=new Label("Vehicle leaves in:" + j);
			    Label label2=new Label(" Current customer ref: " + tempT.getRef());
			       
			    label1.setBounds(50, 200, 150, 20);
			    label2.setBounds(50, 100, 150, 20);
			    f.add(label1);
			    f.add(label2);
			    f.setSize(300,300);
			    f.setVisible(true);
				  
				try // this breaks at some point
				{
					TimeUnit.SECONDS.sleep(1); // wait for user duration then allow new ticket
				} 
				catch (InterruptedException e)
				{
					//TODO Auto-generated catch block
					e.printStackTrace();
				}
				f.setVisible(false);
			}
			i++;
			System.out.println("Vehicle Left. Spaces: " + i); // for debugging
		}		
	}
}
