package so.henry.bookingComponent;

public class ETicket
{
	private String ref;
	private int duration;
	
	public ETicket(String r, int d)
	{
		ref = r;
		duration = d;
	}

	/**
	 * @return the ref
	 */
	public String getRef()
	{
		return ref;
	}

	/**
	 * @param ref the ref to set
	 */
	public void setRef(String ref)
	{
		this.ref = ref;
	}

	/**
	 * @return the duration
	 */
	public int getDuration()
	{
		return duration;
	}

	/**
	 * @param duration the duration to set
	 */
	public void setDuration(int duration)
	{
		this.duration = duration;
	}
}
