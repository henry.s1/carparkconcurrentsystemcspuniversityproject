package so.henry.bookingComponent;

import org.jcsp.lang.CSProcess;
import org.jcsp.lang.ChannelInput;

public class BookingOutputTest implements CSProcess
{
	ChannelInput testInput;
	
	BookingOutputTest(ChannelInput input)
	{
		this.testInput = input;
	}
	
	
	@Override
	public void run()
	{
		while(true)
		{

			ETicket testTicket = (ETicket) testInput.read();
			System.out.println("Ticket Reference" + testTicket.getRef() + "Ticket Duration" + testTicket.getDuration());
		}
	}
	
}
