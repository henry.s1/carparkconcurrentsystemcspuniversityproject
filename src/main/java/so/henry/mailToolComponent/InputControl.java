package so.henry.mailToolComponent;

import java.awt.Frame;
import java.awt.Label;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.jcsp.lang.CSProcess;
import org.jcsp.lang.ChannelInput;
import org.jcsp.lang.ChannelOutput;

import so.henry.bookingComponent.ETicket;

public class InputControl implements CSProcess
{
	ChannelInput fromBag; 
	ChannelOutput toDispatch;	
	ArrayList<ETicket> mailArr = new ArrayList<ETicket>();	
	public InputControl(ChannelInput fromBag, ChannelOutput toDispatch)
	{
		this.fromBag = fromBag;
		this.toDispatch = toDispatch;
	}
	
	@Override
	public void run()
	{
		int i = 0;
		while(true)
		{
			ETicket tempTicket = (ETicket) fromBag.read();
			if(!tempTicket.equals(null))
			{
				mailArr.add(tempTicket);
			}
			
			Frame f=new Frame("Parking display");
			Label label1=new Label("GotTicket" + mailArr.get(i).getRef());
		       
		    label1.setBounds(50, 200, 150, 20);
		    f.add(label1);
		    f.setSize(300,300);
		    f.setVisible(true);
			String userI = JOptionPane.showInputDialog("Options \n 1)next letter \n 2)prev letter 3) del letter and "
					+ "get next \n 4) use for park(for no choice blank)"); //in.nextLine();
			int choice = -1;
			try{choice = Integer.parseInt(userI); }catch(NumberFormatException ne){}
			f.setVisible(false);
			ETicket displayTicket = mailArr.get(0);
			switch(choice)
			{
				case 1:
					if(i + 1 < mailArr.size())
					{
						i++;
					}
					break;
				case 2:
					if(i - 1 > 0)
					{
						i--;
					}
					break;
				case 3:
					mailArr.remove(i);
					if(mailArr.size() != 0)
					{
						i++;
					}
					break;
				case 4:
					toDispatch.write(displayTicket); // write to dispatch
					break;
				case -1: // do nothing
					break;
				default: // if wrong answer do nothing
					break;
			}
			
			
		}
		
		
		
		
		
	}

	
	
}