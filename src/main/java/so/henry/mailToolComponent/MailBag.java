package so.henry.mailToolComponent;

import java.awt.Frame;
import java.awt.Label;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.jcsp.lang.CSProcess;
import org.jcsp.lang.ChannelInput;
import org.jcsp.lang.ChannelOutput;

import so.henry.bookingComponent.ETicket;

public class MailBag implements CSProcess
{

	ChannelInput fromBooking; 
	ChannelOutput toControl;
	
	
	public MailBag(ChannelOutput toControl, ChannelInput fromBooking)
	{
		this.fromBooking = fromBooking;
		this.toControl = toControl;
	}
	
	
	@Override
	public void run()
	{
		int i = 0;
		ArrayList<ETicket> mailArr = new ArrayList<ETicket>();
		ETicket tempTicket1 = new ETicket("Hello", 4);
		ETicket tempTicket2 = new ETicket("no", 4);
		mailArr.add(tempTicket1);
		mailArr.add(tempTicket2);
		while(true)
		{
			//ETicket tempTicket = (ETicket) fromBooking.read();
			if( i < 2)
			{
				ETicket tempTicket = mailArr.get(i);
				i++;
				
				Frame f=new Frame("Parking display");
				Label label1=new Label("Got Ticket, Reference is : " + tempTicket.getRef());
			       
			    label1.setBounds(50, 200, 150, 20);
			    f.add(label1);
			    f.setSize(300,300);
			    f.setVisible(true);
				
				toControl.write(tempTicket);
				
				try 
				{
					TimeUnit.SECONDS.sleep(3); // wait for user duration then allow new ticket
				} 
				catch (InterruptedException e)
				{
					//TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				f.setVisible(false);
			}
		}
	}

}
	
